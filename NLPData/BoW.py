import os
import re
import time

print 'Reading Labels...',
start = time.time()

#################### Reading Labels ###########################
os.chdir("Assignments")
LabelFileNames = ["train.strict.gender", "dev.gender", "test.gender"]
LabelDicts = [{},{},{}];

for i in range(0,len(LabelFileNames)):
	LabelFileName = LabelFileNames[i]
	LabelFile = open(LabelFileName)
	LineList = LabelFile.readlines()
	for line in LineList:
		WordList = line.split()
	#	print WordList
	#	if WordList[1] in LabelDict:
	#		print WordList,
	#		print[LabelDict[WordList[1]]]
		LabelDicts[i][WordList[1]] = WordList[0]
	#print len(LabelDicts[i])



#cntr = 0
#for DocName in LabelDict.keys():
#	cntr += 1
#	if cntr < 15:
#		print DocName,
#		print LabelDict[DocName]

#print len(LabelDict)

###################################################################

print 'DONE \t It took', time.time()-start, 'seconds.'





print 'Creating BoW features...',
start = time.time()


######################## Creating BoW features ###################

from nltk.stem.porter import PorterStemmer
import numpy

st = PorterStemmer()

os.chdir("../Papers")

featuresDictSets = [[],[],[]]
LabelSets = [[],[],[]]
Vocab = []
#cntr = 0
#NumDocs = 9

for fileName in os.listdir('.'):
#	if cntr <NumDocs:
		File = open(fileName)
		fileName = fileName.split(".")[0]

		for i in range(0,len(LabelDicts)):
			if fileName in LabelDicts[i]:
				#print fileName,
				#print LabelDict[fileName]
#				cntr+=1
				LabelSets[i].append(LabelDicts[i][fileName])

				featureDict = {}
				LineList = File.readlines()
				for line in LineList:
						WordList = line.split()
						for word in WordList:
							word = word.split("/")[0]
							word = word.lower()
							word = st.stem(word)
							if word not in featureDict:
								featureDict[word] = 1
							else:
								featureDict[word] += 1

				featuresDictSets[i].append(featureDict)
				Vocab.extend(featureDict.keys())


Vocab = list(set(Vocab))
#print len(Vocab)
#print len(featuresDictSets)



# Converting Dictionary sets to feature vectors

FeatureSets = [[],[],[]];

i = 0;
for featuresDictSet in featuresDictSets:
	for featureDict in featuresDictSet:
		Feature = [];
		#	print len(featureDict.keys())
		for word in Vocab:
			if word in featureDict.keys():
				Feature.append(featureDict[word])
			else:
				Feature.append(0)
		FeatureSets[i].append(Feature)
	i = i + 1;
	
for FeatureSet in FeatureSets:
	FeatureSet = numpy.array(FeatureSet)

for LabelSet in LabelSets:
	LabelSet = numpy.array(LabelSet)

xTrain, yTrain = FeatureSets[0], LabelSets[0]
xTest, yTest = FeatureSets[2], LabelSets[2]

#####################################################################


print 'DONE \t It took', time.time()-start, 'seconds.'




################################ Train SVM ##########################

import resource
resource.setrlimit(resource.RLIMIT_AS, (megs * 1048576L, -1L))

from sklearn import svm

print 'No. of BoW features = ', len(Vocab)
print 'No. of training datapoints = ', len(xTrain)
print 'Training SVM...',

start = time.time()
BoW_clf = svm.SVC(verbose = False)
BoW_clf.fit(xTrain, yTrain)
print 'DONE \t It took', time.time()-start, 'seconds.'

# Predicting on Training Set
yTrain_pred = BoW_clf.predict(xTrain)
TrainAcc = float(sum(yTrain == yTrain_pred)) / float(len(yTrain))
print 'Training Set Accuracy = ', TrainAcc * 100, '%'


############################### Pickle the model #####################
#import pickle
#BoW_Attempt1_641 = pickle.dumps(BoW_clf)
# BoW_clf = pickle.loads(BoW_Attempt1_641)

############################## Predicting on Test Set

yTest_pred = BoW_clf.predict(xTest)
TestAcc = float(sum(yTest == yTest_pred)) / float(len(yTest))
print 'Test Set Accuracy = ', TestAcc * 100, '%'