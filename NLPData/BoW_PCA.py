import pickle
import time
import numpy


print 'Reading Pickled Dictionaries...',
start = time.time()

DictSet_pkl_file = open('FeaturesDictSets.pkl', 'rb')
featuresDictSets = pickle.load(DictSet_pkl_file)

print 'DONE \t It took', time.time()-start, 'seconds.'




print 'Reading Pickled Labels...',
start = time.time()

Labels_pkl_file = open('Labels.pkl', 'rb')
LabelSets = pickle.load(Labels_pkl_file)

print 'DONE \t It took', time.time()-start, 'seconds.'


print 'Reading Pickled Vocab...',
start = time.time()

Vocab_pkl_file = open('Vocab.pkl', 'rb')
Vocab = pickle.load(Vocab_pkl_file)

print 'DONE \t It took', time.time()-start, 'seconds.'






print 'Converting Vocab to Dictionary...',
start = time.time()

VocabDict = {};
i = 0
for word in Vocab:
	VocabDict[word] = i
	i = i + 1



print 'DONE \t It took', time.time()-start, 'seconds.'









print 'Converting Dictionaries to feature vectors',
start = time.time()


FeatureSets = [[],[],[]];
for i in range(0,3):
	FeatureSets[i] = numpy.zeros((len(featuresDictSets[i]),len(Vocab)), dtype=numpy.int)


i = 0;
for featuresDictSet in featuresDictSets:
	j = 0
	for featureDict in featuresDictSet:
		# Feature = numpy.zeros((len(Vocab),), dtype=numpy.int)
		# print len(featureDict.keys())
		for word in featureDict.keys():
			num = VocabDict[word]
			FeatureSets[i][j][num] = featureDict[word]
		j = j + 1
	i = i + 1;


#FeatureSet = []
#for featureDict in featuresDictSets[2]: #Change this 2 to 0, 2 is for testing time taken
#	Feature = numpy.zeros((len(Vocab),), dtype=numpy.int)
	#	print len(featureDict.keys())
#	for word in featureDict.keys():
#		num = VocabDict[word]
#		Feature[num] = featureDict[word]
#	FeatureSet.append(Feature)

print 'DONE \t It took', time.time()-start, 'seconds.'





print 'Preparing Data for SVM....',
start = time.time()


#for FeatureSet in FeatureSets:
#	FeatureSet = numpy.array(FeatureSet)

for LabelSet in LabelSets:
	LabelSet = numpy.array(LabelSet)


xTrain, yTrain = FeatureSets[0], LabelSets[0]
xTest, yTest = FeatureSets[2], LabelSets[2]

#FeatureSet = numpy.array(FeatureSet)
#LabelSet = numpy.array(LabelSets[2])

print 'DONE \t It took', time.time()-start, 'seconds.'



#Vocab_pkl_file = open('Vocab.pkl', 'rb')
#Vocab_pkled = pickle.load(Vocab_pkl_file)



################################ Train SVM ##########################

from sklearn import svm

print 'No. of BoW features = ', len(Vocab)
print 'No. of training datapoints = ', len(xTrain)
print 'Training SVM...',

start = time.time()
BoW_clf = svm.SVC(verbose = False)
BoW_clf.fit(xTrain, yTrain)
print 'DONE \t It took', time.time()-start, 'seconds.'

# Predicting on Training Set
yTrain_pred = BoW_clf.predict(xTrain)
TrainAcc = float(sum(yTrain == yTrain_pred)) / float(len(yTrain))
print 'Training Set Accuracy = ', TrainAcc * 100, '%'


############################### Pickle the model #####################
#import pickle
#BoW_Attempt1_641 = pickle.dumps(BoW_clf)
# BoW_clf = pickle.loads(BoW_Attempt1_641)

############################## Predicting on Test Set

yTest_pred = BoW_clf.predict(xTest)
TestAcc = float(sum(yTest == yTest_pred)) / float(len(yTest))
print 'Test Set Accuracy = ', TestAcc * 100, '%'